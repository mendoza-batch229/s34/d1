const express = require ("express");

const app = express();
const port = 3000;

app.use(express.json());

app.use(express.urlencoded({extended:true}));

// [SECTION] - Routes

// Get method
app.get("/",(req,res) => {
	res.send("Hello Alz"); 
});

// POST method
app.post("/hello",(req,res) => {
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`); 
})


// MOCK DB
// an array that will store user object when the "/signup" route is accessed

let users = [];

app.post("/signup",(req,res) =>{
	console.log(req.body);

	if(req.body.username !== "" && req.body.password !== ""){
		users.push(req.body);
		res.send(`User ${req.body.username} successfully registered!`);
	}else{
		res.send("Please input both username or password");
	}
});


// PUT UPDATE METHOD
app.put("/change-password",(req,res) => {
	console.log(req.body);
	let message;

	for(let i = 0; i < users.length; i++){
		if(req.body.username == users[i].username){
			users[i].password = req.body.password;

			message = `User ${req.body.username}'s password has been updated.`;
			break;
		}else{
			message = "User does not exist.";
		}
	}
	res.send(message);
});


// CODE ALONG ACTIVITY
// Homepage
app.get("/home",(req,res) =>{
	res.send("Welcome to Homepage");
});

// users
app.get("/users",(req,res) =>{
	console.log(req.body);
		res.send(users);

});

// DELETE USER
app.delete("/delete",(req,res) => {
	console.log(req.body);
	let message;
	if(users.length != 0){
		for(let i = 0; i < users.length; i++){
			if(req.body.username == users[i].username){
				users.splice(users[i], 1);
				message = `User ${req.body.username} has been deleted`;
				break;
			}else{
				message = "User does not exist";
			}
		}
	}
	res.send(message);
})




app.listen(port,() => console.log(`Server is running at port: ${port}`));